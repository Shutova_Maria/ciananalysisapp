import time
from copy import copy
from sklearn.cluster import KMeans
import pandas
import matplotlib.pyplot as plt
import pymongo
from PyQt6.QtWidgets import QApplication, QMainWindow, QPushButton, QLineEdit
import cianparser


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Cian Analytics App")
        self.buttonParse = QPushButton("Press to Parse Cian \n to here ↓", self)
        self.buttonForecast = QPushButton("Press for Forecast", self)
        self.buttonStat = QPushButton("Press for Statistics", self)
        self.buttonClass = QPushButton("Press for Classification", self)
        self.buttonDepend = QPushButton("Press for Dependency", self)
        self.buttonStat2 = QPushButton("Press for Second Statistics", self)
        self.textbox = QLineEdit(self)
        self.textbox.setGeometry(110, 90, 170, 40)
        self.textbox.setText('mongodb://localhost:27017')
        #   self.labelProcess = QLabel(self)
        #  self.labelProcess.setGeometry(40, 135, 300, 40)
        #  self.labelProcess.setText("The process is pending, please don't close the app")
        #  self.labelProcess.setHidden(1)
        # self.labelProcess.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.buttonParse.setGeometry(120, 40, 150, 50)
        self.buttonForecast.setGeometry(120, 165, 150, 50)
        self.buttonStat.setGeometry(120, 225, 150, 50)
        self.buttonStat2.setGeometry(120, 285, 150, 50)
        self.buttonDepend.setGeometry(120, 345, 150, 50)
        self.buttonClass.setGeometry(120, 405, 150, 50)
        self.setFixedSize(400, 495)
        self.buttonParse.clicked.connect(self.scanCian)
        self.buttonForecast.clicked.connect(self.forecastTask)
        self.buttonStat.clicked.connect(self.statisticTask)
        self.buttonClass.clicked.connect(self.classificationTask)
        self.buttonDepend.clicked.connect(self.dependencyTask)
        self.buttonStat2.clicked.connect(self.statisticTask2)

    def scanCian(self):
        firstPage = 0
        lastPage = 3
        data = []
        #   self.labelProcess.setHidden(0)
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        while lastPage <= 56:
            data = cianparser.parse(
                deal_type="rent_long",
                accommodation_type="flat",
                # rooms="1",
                location="Санкт-Петербург",
                start_page=firstPage,
                end_page=lastPage,
                is_saving_csv=False)
            firstPage = lastPage + 1
            lastPage = lastPage + 3
            if len(data) > 0:
                information.insert_many(data)
            time.sleep(6)

    def forecastTask(self):
        minFloor = 0
        maxFloor = 0
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        priceMetersStoryList = information.find({}, {'_id': 0, 'floors_count': 1, 'price_per_m2': 1})
        minCopy = copy(priceMetersStoryList)
        maxCopy = copy(priceMetersStoryList)
        minFloorCurs = minCopy.sort('floors_count', pymongo.ASCENDING).limit(1)
        for docMin in minFloorCurs:
            minFloor = docMin['floors_count']
        maxFloorCurs = maxCopy.sort('floors_count', pymongo.DESCENDING).limit(1)
        for docMax in maxFloorCurs:
            maxFloor = docMax['floors_count']
        df1 = pandas.DataFrame()
        avg = 0
        totalHouses = 0
        i = minFloor
        while (i < maxFloor):
            for j in priceMetersStoryList:
                if (j['floors_count'] == i):
                    avg = avg + j['price_per_m2']
                    totalHouses = totalHouses + 1
            if (totalHouses > 0):
                avg = avg / totalHouses
                df2 = pandas.DataFrame({i: [avg]})
                df1 = pandas.concat([df1, df2], axis=1)
                avg = 0
                totalHouses = 0
            i = i + 1
            priceMetersStoryList = copy(priceMetersStoryList)
        df1.plot(position=0.006, xlabel='Floors Count', ylabel='Price per m^2',
                 title='Forecast for price of apartments in bigger buildings', kind="bar")
        plt.show()

    def statisticTask(self):
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        allUndergroundList = list((information.find({}, {'_id': 0, 'underground': 1})))
        distUndergroundList = list((information.find({}, {'_id': 0, 'underground': 1})).distinct('underground'))
        df1 = pandas.DataFrame()
        counter = 0
        for i in distUndergroundList:
            for j in allUndergroundList:
                if i == j['underground']:
                    counter = counter + 1
            if counter > len(allUndergroundList) / len(distUndergroundList):
                df2 = pandas.DataFrame({i: [counter]})
            else:
                df2 = pandas.DataFrame({' ': [counter]})
            df1 = pandas.concat([df1, df2], axis=1)
            counter = 0
            allUndergroundList = copy(allUndergroundList)
        df1 = df1.T
        df1.columns = ['  ']
        df1 = df1.sort_values(by='  ', ascending=True)
        df1.plot(kind="pie", fontsize=6, subplots=True, legend=False)
        plt.show()

    def statisticTask2(self):
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        allUndergroundList = list((information.find({}, {'_id': 0, 'author_type': 1})))
        distUndergroundList = list((information.find({}, {'_id': 0, 'author_type': 1})).distinct('author_type'))
        df1 = pandas.DataFrame()
        counter = 0
        for i in distUndergroundList:
            for j in allUndergroundList:
                if i == j['author_type']:
                    counter = counter + 1
            df2 = pandas.DataFrame({i: [counter]})
            df1 = pandas.concat([df1, df2], axis=1)
            counter = 0
            allUndergroundList = copy(allUndergroundList)
        df1 = df1.T
        df1.columns = ['  ']
        df1 = df1.sort_values(by='  ', ascending=True)
        df1.plot(kind="pie", fontsize=6, subplots=True, legend=False)
        plt.show()

    def dependencyTask(self):
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        priceMetersUndergroundList = information.find({}, {'_id': 0, 'underground': 1, 'price_per_m2': 1})
        undergroundList = (information.find({}, {'_id': 0, 'underground': 1})).distinct('underground')
        df1 = pandas.DataFrame()
        avg = 0
        totalHouses = 0
        for i in undergroundList:
            for j in priceMetersUndergroundList:
                if (j['underground'] == i):
                    avg = avg + j['price_per_m2']
                    totalHouses = totalHouses + 1
            if (totalHouses > 0):
                avg = avg / totalHouses
                df2 = pandas.DataFrame({i: [avg]})
                df1 = pandas.concat([df1, df2], axis=1)
                avg = 0
                totalHouses = 0
            priceMetersUndergroundList = copy(priceMetersUndergroundList)
        df1 = df1.T
        df1.columns = ['price']
        df1 = df1.sort_values(by='price', ascending=True)
        df1.plot(position=0.006, xlabel='Metro Station', ylabel='Price per m^2', fontsize=5, colormap='Accent',
                 title='How price per m^2 dependant from metro?', kind="barh")
        plt.show()

    def classificationTask(self):
        client = pymongo.MongoClient(self.textbox.text())
        mydb = client['CianPosts']
        information = mydb.CianPostsConnection
        df1 = pandas.DataFrame(list(information.find({}, {'_id': 0, 'rooms_count': 1, 'price_per_m2': 1})))
        kmeans = KMeans(n_clusters=5)
        label = kmeans.fit(df1)
        df1['cl'] = kmeans.labels_
        df1.plot.scatter(x='price_per_m2', y='rooms_count', c='cl', colormap='Accent')
        plt.show()


app = QApplication([])
window = MainWindow()
window.show()
app.exec()